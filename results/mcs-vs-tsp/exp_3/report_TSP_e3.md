# TSP report Experiment 3

## Utilizations distribution for each core

![ALT](./TSP_util_for_each_core.png)

| Average utilizations | Variance utilizations | Min | Max |
| ------ | ------ | ------ | ------ |
| 0.503 | 0.003 | 0.371 | 0.695 |

## Utilizations distribution for each system, i.e. for both cores

![ALT](./TSP_util_for_each_system.png)

| Average utilizations | Variance utilizations | Min | Max |
| ------ | ------ | ------ | ------ |
| 1.006 | 0.012 | 0.817 | 1.337 |

