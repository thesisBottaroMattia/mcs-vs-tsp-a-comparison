# TSP report Experiment 1

## Utilizations distribution for each core

![ALT](./TSP_util_for_each_core.png)

| Average utilizations | Variance utilizations | Min | Max |
| ------ | ------ | ------ | ------ |
| 0.565 | 0.002 | 0.420 | 0.673 |

## Utilizations distribution for each system, i.e. for both cores

![ALT](./TSP_util_for_each_system.png)

| Average utilizations | Variance utilizations | Min | Max |
| ------ | ------ | ------ | ------ |
| 1.1297 | 0.005 | 0.912 | 1.277 |

