# TSP report Experiment 4

## Utilizations distribution for each core

![ALT](./TSP_util_for_each_core.png)

| Average utilizations | Variance utilizations | Min | Max |
| ------ | ------ | ------ | ------ |
| 0.577 | 0.001 | 0.498 | 0.650 |

## Utilizations distribution for each system, i.e. for both cores

![ALT](./TSP_util_for_each_system.png)

| Average utilizations | Variance utilizations | Min | Max |
| ------ | ------ | ------ | ------ |
| 1.154 | 0.002 | 1.061 | 1.265 |

