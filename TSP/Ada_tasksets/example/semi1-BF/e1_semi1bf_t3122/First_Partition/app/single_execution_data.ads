package Single_Execution_Data is
	pragma Preelaborate;

	Experiment_Hyperperiod : Natural := 1_000_000;  --  Microseconds

	Delay_Time : Natural := 2_000_000;  --  Microseconds

end Single_Execution_Data;